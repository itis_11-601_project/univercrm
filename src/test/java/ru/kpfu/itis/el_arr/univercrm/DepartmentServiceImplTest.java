package ru.kpfu.itis.el_arr.univercrm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.el_arr.univercrm.model.Department;
import ru.kpfu.itis.el_arr.univercrm.repository.DepartmentRepository;
import ru.kpfu.itis.el_arr.univercrm.service.DepartmentService;

import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UniverCRMApplication.class)
public class DepartmentServiceImplTest {

    @MockBean
    private DepartmentRepository departmentRepository;

    @Autowired
    private DepartmentService departmentService;

    @Test
    public void whenGetAllDepartments_thenReturnListOfDepartments() {
        Department department1 = Department.builder().id(1L).description("new department1").build();
        Department department2 = Department.builder().id(2L).description("new department2").build();
        ArrayList<Department> departments = new ArrayList<>();
        departments.add(department1);
        departments.add(department2);
        Mockito.when(departmentRepository.findAll()).thenReturn(departments);

        assertThat(departmentService.getAllDepartments().size()).isEqualTo(2);
    }

    @Test
    public void whenAddDepartment_thenDepartmentAdded() {
        Mockito.when(departmentRepository.save(Mockito.any(Department.class)))
                .thenAnswer((Answer<Department>) invocationOnMock -> {
                    Object[] arguments = invocationOnMock.getArguments();
                    if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                        Department addedDepartment = (Department) arguments[0];
                        addedDepartment.setId(1L);
                        return addedDepartment;
                    }
                    return null;
                });
        Department added = departmentService.addDepartment(
                "department", "department", "department"
        );
        assertThat(added.getId()).isNotNull();
    }

    @Test
    public void whenGetDepartmentById_thenReturnDepartmentWithRequestedId() {
        Long departmentId = 1L;
        Department department = Department.builder().id(departmentId).description("new department1").build();
        Mockito.when(departmentRepository.findById(department.getId())).thenReturn(Optional.of(department));
        Department found = departmentService.getDepartmentById(departmentId);

        assertThat(found.getId()).isEqualTo(departmentId);
    }
}
