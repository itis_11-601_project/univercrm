function load_ticket_employee() {
    var e = document.getElementById("ticket");
    var value = e.options[e.selectedIndex].value;
    $.ajax({
        data: {"id": value},
        dataType: 'json',
        url: "/ticket-search",
        type: 'POST',
        success: function (result) {
            document.getElementById("id").setAttribute("value", $("#ticket").val());
            document.getElementById("sender").setAttribute("value", result[2]);
            document.getElementById("sender_field").innerHTML =
                "<a style='color:whitesmoke' href='/enrollee/" + result[2] + "'>" + result[3] + "</a>";
            document.getElementById("receiver_field").innerHTML =
                "<a style='color:whitesmoke' href='/department/" + result[4] + "'>" + result[5] + "</a>";
            document.getElementById("receiver").setAttribute("value", result[4]);
            document.getElementById("title_field").innerHTML = result[6];
            document.getElementById("title").setAttribute("value", result[6]);
            document.getElementById("content_field").innerHTML = result[7];
            document.getElementById("content").setAttribute("value", result[7]);
            document.getElementById("answer").innerHTML = result[8];
            $("#status").val(result[9]);
        },
        error: function (e) {
            console.log("error: " + e);
        }
    })
}