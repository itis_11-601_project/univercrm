function load_ticket_enrollee() {
    var e = document.getElementById("ticket");
    var value = e.options[e.selectedIndex].value;
    if (value === '0') {
        null_case();
    } else {
        notnull_case();
    }
}

function notnull_case() {
    var e = document.getElementById("ticket");
    var id = e.options[e.selectedIndex].value;
    $.ajax({
        data: {"id": id},
        dataType: 'json',
        url: "/ticket-search",
        type: 'POST',
        success: function (result) {
            document.getElementById("id").setAttribute("value", $("#ticket").val());
            document.getElementById("sender").setAttribute("value", result[2]);
            document.getElementById("sender_field").innerHTML =
                "<a style='color:whitesmoke' href='/enrollee/" + result[2] + "'>" + result[3] + "</a>";
            document.getElementById("receiver_field").innerHTML =
                "<a style='color:whitesmoke' href='/department/" + result[4] + "'>" + result[5] + "</a>";
            document.getElementById("receiver").setAttribute("value", result[4]);
            document.getElementById("title").setAttribute("value", result[6]);
            document.getElementById("content").setAttribute("value", result[7]);
            document.getElementById("answer_field").innerHTML = result[8];
            document.getElementById("answer").setAttribute("value", result[8]);
            document.getElementById("status_field").innerHTML = result[9];
            document.getElementById("status").setAttribute("value", result[9]);
            $("#department").val(result[4]);
        },
        error: function (e) {
            console.log("error: " + e);
        }
    })
}