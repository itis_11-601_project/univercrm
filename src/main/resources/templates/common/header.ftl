<header id="header">
    <div class="inner">
        <a href="/" class="logo"><strong>The 2ка</strong> CRM</a>
        <nav id="nav">
            <a href="/">The 2ка</a>
            <a href="/departments">Факультеты</a>
            <a href="/contacts">Информация</a>
            <#if model??>
                <#if model.user??>
            <a href="/user">
                    <#if model.user == true>
                        Личный кабинет</a>
            <a href="/logout">Выйти</a>
                    <#else>
                Войти</a>
                    </#if>
                </#if>
            </#if>
        </nav>
    </div>
</header>