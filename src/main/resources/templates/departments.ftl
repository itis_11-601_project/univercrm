<!DOCTYPE HTML>
<html>

<head>
    <title>Факультеты</title>
    <#include "common/head.ftl">
</head>

<body class="subpage" onload="search_department()">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>Факультеты</h2>
    <#if model.departments == true>
            <p>Ниже представлены все виды факультетов, обитающих в 2ке</p>

        <#include "common/ajax-search-department.ftl">

        </header>

        <div id="results"></div>

        <section class="3u$ 6u$(medium)">
            <a href="/departments/download" class="button special">Сохранить список факультетов</a>
        </section>
        <br/>
    <#else>
            <p>К сожалению, в базе данных нет никаких факультетов, обитающих в 2ке</p>
        </header>
    </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
            <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
