<!DOCTYPE HTML>
<html>

<head>
    <title>Администратор/Пользователи</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<footer id="footer">
    <div class="inner">

        <h3>Пользователи</h3>
        <br/>

    <#if model.user_data??>
        <#list model.user_data as data>

        <h2>Состояние</h2>
        <select name="status" id="status" form="form${data.getRelatedUser().getId()}">
            <option value="${data.getStatus().name()}">
                ${data.getStatus().name()}
            </option>
            <#list model.statuses as status>
                <#if status == data.getStatus().name()>
                    <#continue>
                </#if>
                <option value="${status}">${status}</option>
            </#list>
        </select>
        <br/>

        <h2>Роль</h2>
        <select name="role" id="role" form="form${data.getRelatedUser().getId()}">
            <option value="${data.getRelatedUser().getRole().name()}">
                ${data.getRelatedUser().getRole().name()}
            </option>
            <#list model.roles as role>
                <#if role == data.getRelatedUser().getRole().name()>
                    <#continue>
                </#if>
                <option value="${role}">${role}</option>
            </#list>
        </select>
        <br/>

        <form action="/admin/users/edit" method="post" id="form${data.getRelatedUser().getId()}"
            ">
            <input type="hidden" name="id" id="id" value="${data.getRelatedUser().getId()}">

            <div class="field half first">
                <label for="first_name">Имя</label>
                <input name="first_name" id="first_name" type="text" placeholder="Имя"
                       value="${data.getRelatedUser().getFirstName()}"/>
            </div>
            <div class="field half">
                <label for="second_name">Фамилия</label>
                <input name="second_name" id="second_name" type="text" placeholder="Фамилия"
                       value="${data.getRelatedUser().getSecondName()}"/>
            </div>

            <div class="field half first">
                <label for="age">Возраст</label>
                <input name="age" id="age" type="text" placeholder="Ответ"
                       value="${data.getRelatedUser().getAge()}"/>
            </div>
            <div class="field half">
                <label for="contact">Почтовый ящик</label>
                <input name="contact" id="contact" type="text" placeholder="Ответ"
                       value="${data.getRelatedUser().getContact()}">
            </div>

            <input value="Изменить" class="button alt" type="submit">
        </form>

        <form action="/admin/users/delete" method="post">
            <input type="hidden" name="id" id="id" value="${data.getRelatedUser().getId()}">
            <input value="Удалить" class="button alt" type="submit">
        </form>
        <hr class="major"/>

        </#list>
    <#else>

        <h2>Заявок нет в базе!</h2>

    </#if>

    <#include "common/copyright.ftl">

    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
