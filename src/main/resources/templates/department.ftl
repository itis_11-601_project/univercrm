<!DOCTYPE HTML>
<html>

<head>
    <#if model.department??>
    <title>${model.department.getName()}</title>
    <#else>
    <title>Факультеты. Ошибочка вышла.</title>
    </#if>
    <#include "common/head.ftl">
</head>

<body class="subpage" onload="search_specialities()">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">
    <header class="align-center">
        <#if model.department??>
            <h2 id="d_id">${model.department.getName()}</h2>
            <p>${model.department.getDescription()}</p>
            <p>Email: ${model.department.getEmail()}</p>
            <p>Телефон: ${model.department.getPhone_num()}</p>
            <#if model.specialities == true>
            <p>Ниже представлены специальности данного факультета</p>
        </header>

                <#include "common/ajax-search-specialities.ftl">

        <div id="results"></div>

        <section class="3u$ 6u$(medium)">
            <a href="/department/${model.department.getId()}/download" class="button special">Сохранить список
                специальностей</a>
        </section>
        <br/>
            <#else>
            <p>К сожалению, в базе данных нет специальностей для данного факультета</p>
        </header>
            </#if>
        <#else>
            <h2>Извините, о данном факультете нет никакой информации.</h2>
            <p>Попробуйте поискать еще.</p>
        </header>
        </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
