<!DOCTYPE HTML>
<html>

<head>
    <title>Контакты</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="three" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>The 2ка</h2>
            <p>Высшее учебное заведение</p>
        </header>

        <div class="image round left">
            <img src="img/pic01.jpg" alt="Pic 01"/>
        </div>
        <p>Казанский (Приволжский) федеральный университет, второй учебный корпус.</p>
        <div></div>
        <div class="image round right">
            <img src="img/pic02.jpg" alt="Pic 02"/>
        </div>
        <p>Будние дни: 08:00–17:00. Обед: 12:00–13:00.</p>

        <p>+7 (843) 233–71–09</p>

    </div>
</section>

<#include "common/google-maps-frame.ftl">

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>