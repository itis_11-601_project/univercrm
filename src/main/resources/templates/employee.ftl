<!DOCTYPE HTML>
<html>

<head>
    <title>Сотрудник</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="three" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>${model.employee.getUser().getFirstName()} ${model.employee.getUser().getSecondName()}</h2>
            <p>${model.employee.getJob_description()}</p>
        </header>

        <div class="image round right">
            <img src="/img/pic02.jpg" alt="Pic 02"/>
        </div>

        <h4>Данные</h4>
        <dl>
            <#if model.meme??>
                <#if model.meme == true>
            <dt>Доступ</dt>
            <dd>
                <p>${model.employee.getUser().getRole()}</p>
            </dd>
                </#if>
            </#if>
            <dt>Имя</dt>
            <dd>
                <p>${model.employee.getUser().getFirstName()}</p>
            </dd>
            <dt>Фамилия</dt>
            <dd>
                <p>${model.employee.getUser().getSecondName()}</p>
            </dd>
            <dt>Возраст</dt>
            <dd>
                <p>${model.employee.getUser().getAge()}</p>
            </dd>
            <dt>Почтовый ящик</dt>
            <dd>
                <p>${model.employee.getUser().getContact()}</p>
            </dd>
            <dt>Описание</dt>
            <dd>
                <p>${model.employee.getJob_description()}</p>
            </dd>
            <dt>Факультет</dt>
            <dd>
                <p>
                <#if model.employee.getDepartment()??>
                    <a href="/department/${model.employee.getDepartment().getId()}">${model.employee.getDepartment().getName()}</a>
                <#else>
                    Сотрудник не закреплён за каким-либо факультетом
                </#if>
                </p>
            </dd>
        </dl>

        <header class="align-center">
            <div class="row">
            <#if model.meme??>
            <#if model.meme == true>
                <div class="6u 12u$(small)">
                    <a href="/employee/${model.employee.getId()}/edit" class="button special">Изменить свои данные</a>
                </div>
                <div class="6u$ 12u$(small)">
                    <a href="/ticket/employee/${model.employee.getId()}/" class="button special">Проверить заявки</a>
                </div>
            </#if>
            </#if>
            </div>
        </header>

    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
