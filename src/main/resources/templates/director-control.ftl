<!DOCTYPE HTML>
<html>

<head>
    <title>Директор</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">
        <header class="align-center">
            <h2>Функции</h2>
            <p>Ниже представлены все функции, доступные вам</p>
        </header>

        <div class="row">
            <section class="6u 12u$(medium)">
                <h2>Мои данные</h2>
                <p>Просмотр и изменение своих данных</p>
            </section>
            <section class="3u$ 6u$(medium)">
                <a href="/employee/me" class="button">Мои данные</a>
            </section>
        </div>

        <hr class="major"/>

        <div class="row">
            <section class="6u 12u$(medium)">
                <h2>Сотрудники</h2>
                <p>Изменение данных о сотрудниках</p>
            </section>
            <section class="3u$ 6u$(medium)">
                <a href="/director/employees" class="button">Сотрудники</a>
            </section>
        </div>

        <hr class="major"/>

        <div class="row">
            <section class="6u 12u$(medium)">
                <h2>Мероприятия</h2>
                <p>Изменение данных о мероприятиях</p>
            </section>
            <section class="3u$ 6u$(medium) 12u$(small)">
                <a href="/director/events" class="button">Мероприятия</a>
            </section>
        </div>

        <hr class="major"/>

        <div class="row">
            <section class="6u 12u$(medium)">
                <h2>Факультеты</h2>
                <p>Изменение данных о факультетах</p>
            </section>
            <section class="3u$ 6u$(medium) 12u$(small)">
                <a href="/director/departments" class="button">Факультеты</a>
            </section>
        </div>

        <hr class="major"/>

        <div class="row">
            <section class="6u 12u$(medium)">
                <h2>Специальности</h2>
                <p>Изменение данных о специальностях</p>
            </section>
            <section class="3u$ 6u$(medium) 12u$(small)">
                <a href="/director/specialities" class="button">Специальности</a>
            </section>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
