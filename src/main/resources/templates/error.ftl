<!DOCTYPE HTML>
<html>

<head>
    <title>Ошибочка ( ͡° ͜ʖ ͡°)</title>
    <#include "common/head.ftl">
</head>

<body>

<#include "common/header.ftl">

<section id="banner">
    <div class="inner">

        <header>
            <h1>
                Похоже, что-то пошло не так.
                <br/>
                Команда дрессированных программистов уже выехала.
            </h1>
        </header>

        <div class="flex ">
            <div>
                <span class="icon fa-car"></span>
                <h3>Факультеты</h3>
                <p>Факультеты, представленные в The 2ка</p>
            </div>

            <div>
                <span class="icon fa-camera"></span>
                <h3>Специальности</h3>
                <p>Специальности The 2ка</p>
            </div>

            <div>
                <span class="icon fa-bug"></span>
                <h3>Заявления</h3>
                <p>Онлайн подача заявлений в The 2ка</p>
            </div>
        </div>

        <footer>
            <a href="/departments" class="button">Поехали!</a>
        </footer>

    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>