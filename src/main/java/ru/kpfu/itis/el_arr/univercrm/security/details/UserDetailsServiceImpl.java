package ru.kpfu.itis.el_arr.univercrm.security.details;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;
import ru.kpfu.itis.el_arr.univercrm.repository.UserLoginDataRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private UserLoginDataRepository userLoginDataRepository;

    public UserDetailsServiceImpl(UserLoginDataRepository userLoginDataRepository) {
        this.userLoginDataRepository = userLoginDataRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserLoginData userLoginData = userLoginDataRepository.findByUsername(username).orElseThrow(
                () -> new IllegalArgumentException("User with username " + username + " doesn't exist!")
        );
        return new UserDetailsImpl(userLoginData);
    }

}
