package ru.kpfu.itis.el_arr.univercrm.service.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.el_arr.univercrm.model.Event;
import ru.kpfu.itis.el_arr.univercrm.repository.EventRepository;
import ru.kpfu.itis.el_arr.univercrm.service.EventService;

import java.util.List;

@Service
public class EventServiceImpl implements EventService {
    private EventRepository eventRepository;

    public EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public void removeEventById(long id) {
        eventRepository.deleteById(id);
    }

    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public Event getEventById(long id) {
        return eventRepository.findById(id).orElse(null);
    }

}
