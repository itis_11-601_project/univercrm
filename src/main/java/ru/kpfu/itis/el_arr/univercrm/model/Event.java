package ru.kpfu.itis.el_arr.univercrm.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder

@Entity
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_seq")
    @SequenceGenerator(name = "event_seq", sequenceName = "SEQ_EVENT", allocationSize = 1)
    private long id;

    private String title;
    private String description;

    @Temporal(TemporalType.DATE)
    private Date date;

}
