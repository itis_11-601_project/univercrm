package ru.kpfu.itis.el_arr.univercrm.service;

import ru.kpfu.itis.el_arr.univercrm.model.Event;

import java.util.List;

public interface EventService {

    //TODO add new Event to database

    void removeEventById(long id);

    List<Event> getAllEvents();

    Event getEventById(long id);

}
