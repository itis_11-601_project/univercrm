package ru.kpfu.itis.el_arr.univercrm.service;

import ru.kpfu.itis.el_arr.univercrm.model.Department;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;

import java.util.List;

public interface EmployeeService {

    void addEmployee(String first_name, String second_name,
                     Department department, String job_description);

    void removeEmployeeById(long id);

    void removeEmployee(Employee employee);

    void editEmployee(long id, String first_name, String second_name,
                      String age, String contact, Department department, String job_description);

    List<Employee> getAllEmployees();

    Employee getEmployeeById(long id);

    Employee getEmployeeByUserId(long id);

    List<Employee> getEmployeesByDeptId(long id);

}
