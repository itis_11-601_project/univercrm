package ru.kpfu.itis.el_arr.univercrm.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.el_arr.univercrm.model.Ticket;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserRole;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserStatus;
import ru.kpfu.itis.el_arr.univercrm.repository.UserLoginDataRepository;
import ru.kpfu.itis.el_arr.univercrm.repository.UserRepository;
import ru.kpfu.itis.el_arr.univercrm.service.UserInfoService;

import java.util.List;
import java.util.Optional;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    private UserLoginDataRepository userLoginDataRepository;
    private UserRepository userRepository;
    private PasswordEncoder encoder;

    public UserInfoServiceImpl(UserRepository userRepository, PasswordEncoder encoder,
                               UserLoginDataRepository userLoginDataRepository) {
        this.encoder = encoder;
        this.userRepository = userRepository;
        this.userLoginDataRepository = userLoginDataRepository;
    }

    @Override
    public void addUser(String username, String password, String first_name, String second_name,
                        int age, String contact, UserRole role, UserStatus status) {
        User user = User.builder()
                .firstName(first_name)
                .secondName(second_name)
                .age(age)
                .contact(contact)
                .role(role)
                .build();
        UserLoginData userLoginData = UserLoginData.builder()
                .relatedUser(user)
                .role(user.getRole())
                .status(status)
                .username(username)
                .passwordHashed(encoder.encode(password))
                .build();
        userRepository.save(user);
        userLoginDataRepository.save(userLoginData);
    }

    @Override
    public void removeUser(User user) {
        UserLoginData userLoginData = userLoginDataRepository.findByRelatedUser(user).orElse(null);
        if (userLoginData != null) {
            userLoginDataRepository.delete(userLoginData);
            userRepository.delete(user);
        }
    }

    @Override
    public void removeUserById(long id) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            UserLoginData userLoginData = userLoginDataRepository.findByRelatedUser(user).orElse(null);
            if (userLoginData != null) {
                userLoginDataRepository.delete(userLoginData);
                userRepository.delete(user);
            }
        }
    }

    @Override
    public void editUser(long id, String role, String status, String first_name, String second_name,
                         int age, String contact, List<Ticket> ticketsRecieved, List<Ticket> ticketsSent) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            UserLoginData userLoginData = userLoginDataRepository.findByRelatedUser(user).orElse(null);
            if (userLoginData != null) {
                UserRole userRole;
                if (role.equals(UserRole.DIRECTOR.name())) {
                    userRole = UserRole.DIRECTOR;
                } else if (role.equals(UserRole.ADMIN.name())) {
                    userRole = UserRole.ADMIN;
                } else if (role.equals(UserRole.EMPLOYEE.name())) {
                    userRole = UserRole.EMPLOYEE;
                } else {
                    userRole = UserRole.ENROLLEE;
                }
                user.setRole(userRole);
                userLoginData.setRole(userRole);
                UserStatus userStatus = UserStatus.CONFIRMED;
                if (status.equals(UserStatus.BANNED.name())) {
                    userStatus = UserStatus.BANNED;
                } else if (status.equals(UserStatus.UNCONFIRMED.name())) {
                    userStatus = UserStatus.UNCONFIRMED;
                }
                userLoginData.setStatus(userStatus);
                user.setFirstName(first_name);
                user.setSecondName(second_name);
                user.setAge(age);
                user.setContact(contact);
                user.setTicketsSent(ticketsSent);
                user.setTicketsReceived(ticketsRecieved);
                userRepository.save(user);
                userLoginDataRepository.save(userLoginData);
            }
        }
    }

    @Override
    public void editUser(long id, String first_name, String second_name, int age, String contact) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            UserLoginData userLoginData = userLoginDataRepository.findByRelatedUser(user).orElse(null);
            if (userLoginData != null) {
                user.setFirstName(first_name);
                user.setSecondName(second_name);
                user.setAge(age);
                user.setContact(contact);
                userRepository.save(user);
                userLoginDataRepository.save(userLoginData);
            }
        }
    }

    @Override
    public void editUserTickets(User user, List<Ticket> ticketsReceived, List<Ticket> ticketsSent) {
        User result = userRepository.findById(user.getId()).orElse(null);
        if (result != null) {
            List<Ticket> received = result.getTicketsReceived();
            List<Ticket> sent = result.getTicketsSent();
            received.addAll(ticketsReceived);
            sent.addAll(ticketsSent);
            result.setTicketsReceived(received);
            result.setTicketsSent(sent);
            userRepository.save(result);
        }
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User getUserByFullName(String firstName, String secondName) {
        return userRepository.findByFirstNameAndSecondName(firstName, secondName).orElse(null);
    }

    @Override
    public User getUserByUsername(String username) {
        Optional<UserLoginData> userLoginDataOptional = userLoginDataRepository.findByUsername(username);
        return userLoginDataOptional.map(UserLoginData::getRelatedUser).orElse(null);
    }

    @Override
    public List<UserLoginData> getAllUsersLoginData() {
        return userLoginDataRepository.findAll();
    }

    @Override
    public UserLoginData getUserLoginDataByUserId(long id) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null)
            return userLoginDataRepository.findByRelatedUser(user).orElse(null);
        else
            return null;
    }

    @Override
    public UserLoginData getUserLoginDataByUsername(String username) {
        return userLoginDataRepository.findByUsername(username).orElse(null);
    }

}
