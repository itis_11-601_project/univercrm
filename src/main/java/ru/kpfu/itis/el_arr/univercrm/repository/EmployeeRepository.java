package ru.kpfu.itis.el_arr.univercrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {}
