package ru.kpfu.itis.el_arr.univercrm.model;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder

@Entity
@Table(name = "enrollee")
public class Enrollee {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrollee_seq")
    @SequenceGenerator(name = "enrollee_seq", sequenceName = "SEQ_ENROLLEE", allocationSize = 1)
    private long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Lob
    private byte[] diploma;

}
