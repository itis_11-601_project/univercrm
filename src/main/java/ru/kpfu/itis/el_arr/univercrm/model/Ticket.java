package ru.kpfu.itis.el_arr.univercrm.model;

import lombok.*;
import ru.kpfu.itis.el_arr.univercrm.model.constant.TicketStatus;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticket_seq")
    @SequenceGenerator(name = "ticket_seq", sequenceName = "SEQ_TICKET", allocationSize = 1)
    private long id;

    @Temporal(TemporalType.DATE)
    private Date date;

    @OneToOne
    @JoinColumn(name = "sender_id")
    private Enrollee sender;

    @OneToOne
    @JoinColumn(name = "receiver_id")
    private Employee receiver;

    private String title;
    private String content;
    private String answer;

    @Enumerated(EnumType.STRING)
    private TicketStatus status;

}
