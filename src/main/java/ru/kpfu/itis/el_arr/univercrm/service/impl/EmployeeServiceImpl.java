package ru.kpfu.itis.el_arr.univercrm.service.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.el_arr.univercrm.model.Department;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.repository.EmployeeRepository;
import ru.kpfu.itis.el_arr.univercrm.service.EmployeeService;
import ru.kpfu.itis.el_arr.univercrm.service.UserInfoService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;
    private UserInfoService userInfoService;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               UserInfoService userInfoService) {
        this.employeeRepository = employeeRepository;
        this.userInfoService = userInfoService;
    }

    @Override
    public void addEmployee(String first_name, String second_name,
                            Department department, String job_description) {
        User user = userInfoService.getUserByFullName(first_name, second_name);
        Employee employee = Employee.builder()
                .user(user)
                .department(department)
                .job_description(job_description)
                .build();
        employeeRepository.save(employee);
    }

    @Override
    public void removeEmployeeById(long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public void removeEmployee(Employee employee) {
        employeeRepository.delete(employee);
    }

    @Override
    public void editEmployee(long id, String first_name, String second_name,
                             String age, String contact, Department department, String job_description) {
        Employee employee = employeeRepository.findById(id).orElse(null);
        if (employee != null) {
            User user = userInfoService.getUserById(employee.getUser().getId());
            userInfoService.editUser(
                    user.getId(),
                    first_name,
                    second_name,
                    Integer.valueOf(age),
                    contact
            );
            employee.setDepartment(department);
            employee.setJob_description(job_description);
            employeeRepository.save(employee);
        }
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeById(long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public Employee getEmployeeByUserId(long id) {
        for (Employee e : employeeRepository.findAll()) {
            if (e.getUser().getId() == id) {
                return e;
            }
        }
        return null;
    }

    @Override
    public List<Employee> getEmployeesByDeptId(long id) {
        return employeeRepository.findAll().stream()
                .filter(e -> e.getDepartment().getId() == id)
                .collect(Collectors.toList());
    }

}
