package ru.kpfu.itis.el_arr.univercrm.model;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder

@Entity
@Table(name = "speciality")
public class Speciality {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "speciality_seq")
    @SequenceGenerator(name = "speciality_seq", sequenceName = "SEQ_SPEC", allocationSize = 1)
    private long id;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    private String name;
    private String main_description;
    private String small_description_1;
    private String small_description_2;

}
